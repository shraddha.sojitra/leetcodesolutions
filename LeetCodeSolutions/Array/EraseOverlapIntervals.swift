//
//  EraseOverlapIntervals.swift
//  LeetCodeSolutions
//
//  Created by Shraddha Sojitra on 15/08/20.
//  Copyright © 2020 Shraddha Sojitra. All rights reserved.
//

import Foundation

func eraseOverlapIntervals(_ intervals: [[Int]]) -> Int {
    
    if intervals.count < 2 {
        return 0
    }
    
    var intervals = intervals
    intervals.sort { $0[0] < $1[0] }
    
    var result: [[Int]] = []
    
    for (idx, _) in intervals.enumerated() {
        if idx+1 < intervals.count && intervals[idx][1] > intervals[idx+1][0] {
            intervals[idx+1][0] = min(intervals[idx][0], intervals[idx+1][0])
            intervals[idx+1][1] = min(intervals[idx][1], intervals[idx+1][1])
        } else {
            result.append(intervals[idx])
        }
    }
    
    return intervals.count - result.count
    
}
