//
//  TwoSum.swift
//  LeetCodeSolutions
//
//  Created by Shraddha Sojitra on 14/08/20.
//  Copyright © 2020 Shraddha Sojitra. All rights reserved.
//

import Foundation

func twoSum(_ nums: [Int], _ target: Int) -> [Int] {
       for (i, number) in nums.enumerated() {
           for j in 0...nums.count-1 {
               if i != j {
                   if number + nums[j] == target {
                       return [i, j].sorted()
                   }
               } else {
                   break
               }
           }
       }
     return []
   }

