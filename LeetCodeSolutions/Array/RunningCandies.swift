//
//  RunningCandies.swift
//  LeetCodeSolutions
//
//  Created by SHRADDHA on 20/08/20.
//  Copyright © 2020 Shraddha Sojitra. All rights reserved.
//

import Foundation

func distributeCandies(_ candies: Int, _ num_people: Int) -> [Int] {
           
           var arr: [Int] = []
           var remainingCandies = candies
           var numberOfRound = 0
           
           for _ in 0..<num_people {
               arr.append(0)
           }
           
           while remainingCandies > 0  {
               for (i, _) in arr.enumerated() {
                   if numberOfRound > 0 {
                       if remainingCandies <= (num_people*numberOfRound) + (i+1) {
                           arr[i] += remainingCandies
                           remainingCandies = 0
                       } else {
                           arr[i] += (num_people*numberOfRound) + (i+1)
                           remainingCandies -= (num_people*numberOfRound) + (i+1)
                       }
                   } else {
                       if remainingCandies < i+1 {
                           arr[i] += remainingCandies
                           remainingCandies = 0
                       } else {
                           arr[i] += i+1
                           remainingCandies -= i+1
                       }
                   }
                   if remainingCandies <= 0 {
                       break
                   }
               }
               numberOfRound += 1
           }
                       
           print(arr)
           return arr
       }
   
