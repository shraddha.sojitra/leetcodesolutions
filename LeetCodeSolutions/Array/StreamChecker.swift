//
//  StreamChecker.swift
//  LeetCodeSolutions
//
//  Created by SHRADDHA on 24/08/20.
//  Copyright © 2020 Shraddha Sojitra. All rights reserved.
//

import Foundation

//        * Your StreamChecker object will be instantiated and called as such:
let obj = StreamChecker(["StreamChecker","query","query","query","query","query","query","query","query","query","query","query","query","query","query","query","query","query","query","query","query","query","query","query","query","query","query","query","query","query","query"])
 let ret_1: Bool = obj.query("a")


class Node {
    var dict = [Character: Node]()
    var isEnd = false

    init() {}

    init(_ val: Character) {
        dict[val] = Node()
    }

    func insert(_ word: [Character], _ i: Int) {
        self.isEnd = self.isEnd || i == word.count
        if i == word.count { return }

        if let node = dict[word[i]] {
            return node.insert(word, i+1)
        }

        let node =  Node()
        dict[word[i]] = node
        node.insert(word, i+1)
    }
}

class StreamChecker {
    let root = Node()
    lazy var cur = self.root
    var store = [Character]()

    init(_ words: [String]) {
        for word in words {
            root.insert(Array(word).reversed(), 0)
        }
    }


    func query(_ letter: Character) -> Bool {
        store.insert(letter, at: 0)
        var cur = root
        for char in store {
            if let node = cur.dict[char] {
                cur = node
                if node.isEnd { return true}
            } else { break }
        }
        return false
    }
}
