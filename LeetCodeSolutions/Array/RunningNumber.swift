//
//  RunningNumber.swift
//  LeetCodeSolutions
//
//  Created by Shraddha Sojitra on 15/08/20.
//  Copyright © 2020 Shraddha Sojitra. All rights reserved.
//

import Foundation

func runningSum(_ nums: [Int]) -> [Int] {
       
       var resultarray: [Int] = []

       for (index, _) in nums.enumerated() {
           var newValue = nums[index]
           for j in 0..<index {
               newValue += nums[j]
           }
           resultarray.append(newValue)
       }

       return resultarray
   }
