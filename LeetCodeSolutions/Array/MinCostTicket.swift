//
//  MinCostTicket.swift
//  LeetCodeSolutions
//
//  Created by SHRADDHA on 26/08/20.
//  Copyright © 2020 Shraddha Sojitra. All rights reserved.
//

import Foundation

func mincostTickets(_ days: [Int], _ costs: [Int]) -> Int {
     
     var dp = Array(repeating: Int.max, count: (days.count+1))
     dp[0] = 0
     let noofdays = [1,7,30]
     for i in 0..<days.count{
         if dp[i + 1] == Int.max{
             dp[i + 1] = dp[i] + costs[0];
         }
         for j in 0...2{
             for k in i..<days.count{
                 if (days[k] < days[i] + noofdays[j]) {
                     dp[k + 1] = min(dp[i] + costs[j], dp[k + 1]);
                 }else {break}
             }
         }
     }
     return dp[dp.count - 1]
     
 }
