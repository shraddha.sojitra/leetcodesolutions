//
//  ThreeConcecutiveOdds.swift
//  LeetCodeSolutions
//
//  Created by SHRADDHA on 16/08/20.
//  Copyright © 2020 Shraddha Sojitra. All rights reserved.
//

import Foundation

func threeConsecutiveOdds(_ arr: [Int]) -> Bool {
       
       if arr.count < 3 {
           return false
       }
       
       for (index, _) in arr.enumerated() {
           if index < arr.count - 2 {
               if arr[index]%2 != 0 && arr[index+1]%2 != 0 && arr[index+2]%2 != 0 {
                   return true
               }
           }
       }
       
       
       return false
   }
