//
//  GoatLatin.swift
//  LeetCodeSolutions
//
//  Created by Shraddha Sojitra on 19/08/20.
//  Copyright © 2020 Shraddha Sojitra. All rights reserved.
//

import Foundation

func toGoatLatin(_ S: String) -> String {
       
       var newString = ""
       
       for (index,word) in S.components(separatedBy: " ").enumerated() {
           switch word.first {
           case "a", "e", "i", "o", "u", "A", "E", "I", "O", "U":
               newString.append(word)
               newString.append("ma")
           default:
               var newWord = word
               let lastC = newWord.removeFirst()
               newString.append(contentsOf: newWord)
               newString.append(lastC)
               newString.append("ma")
           }
           
           for _ in 0...index{
               newString.append("a")
           }
           newString.append(" ")
       }
       return newString.trimmingCharacters(in: .whitespacesAndNewlines)
   }
   
