//
//  FindLeastNumber.swift
//  LeetCodeSolutions
//
//  Created by Shraddha Sojitra on 15/08/20.
//  Copyright © 2020 Shraddha Sojitra. All rights reserved.
//

import Foundation

func findLeastNumOfUniqueInts(_ arr: [Int], _ k: Int) -> Int {
    
    var counts: [Int: Int] = [:]
    for num in arr {
        counts[num] = (counts[num] ?? 0) + 1
    }
    
    if k == 0 {
        return counts.count
    }
    
    let sortedarr = counts.sorted { $0.value < $1.value }

    var index = 0
    
    for (key, value) in sortedarr {
        for _ in 0..<value {
            if let c = counts[key] {
                counts[key] = c - 1 == 0 ? nil : c - 1
                index += 1
            }
            if index == k {
                return counts.count
            }
        }
    }
    return counts.count
}
